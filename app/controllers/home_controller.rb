class HomeController < ApplicationController
  def index
  	I18n.locale = params[:locale] || I18n.default_locale

    set_meta_tags :title => t('controllers.home.title'),
                  :description => t('controllers.home.description'),
                  :meta => t('controllers.home.meta')
  end
end
