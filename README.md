# README #

The solidfire_international repo is the SolidFire international micro sites. This project is built on rails 4.2.0. Below you will find instructions for getting this repo setup on your local machine and for pushing code to our development and production servers.

### How do I get set up? ###

* Clone this repo to your local machine. git clone git@bitbucket.org:solidfire/solidfire_international.git
* Download and install the [pow web server](http://pow.cx/)
* Make sure you have a version of ruby 2.0.0 installed (ruby -v)
* [Follow these instructions on creating a .powenv file.](http://stackoverflow.com/questions/20199970/bundlerrubyversionmismatch-your-ruby-version-is-1-9-3-but-your-gemfile-speci)
* Run bundle install in the root directory of solidfire_international repo on your local machine.
* Browse to [http://solidfire_international.dev/](http://solidfire_international.dev/) and the rails app should load.