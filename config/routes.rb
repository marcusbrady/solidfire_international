Rails.application.routes.draw do
  get 'storage_system/index'

  root :to => 'home#index'

  match '/', :to => "home#index", :as => :home_index, :via => :get

end
